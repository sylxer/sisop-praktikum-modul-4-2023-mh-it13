#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>

#define CHUNK_SIZE 1024

static const char * dir_path = "/home/syl/modular";
static int is_readdir_called = 0;

void write_log(const char* flag, const char* cmd, const char* desc) {
    time_t now = time(NULL);
    struct tm* tm_info = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    FILE* log_file = fopen("/home/syl/fs_module.log", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        return;
    }

    fprintf(log_file, "%s::%s::%s::%s\n", flag, timestamp, cmd, desc);
    fclose(log_file);
}


static int xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char full_path[256];

    sprintf(full_path, "%s%s", dir_path, path);

    res = lstat(full_path, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

void create_chunk_file(const char *filePath){
    printf("Filepath %s\n", filePath);

    FILE* fp = fopen(filePath, "rb");

    if(fp == NULL){
        printf("Gagal membuka file %s\n", filePath);
        exit(EXIT_FAILURE);
    }

    char chunk_path[256];
    int chunk_count = 0;
    char read_file[CHUNK_SIZE];

    size_t chunk_size;
    while((chunk_size = fread(read_file, 1, CHUNK_SIZE, fp)) > 0){
        sprintf(chunk_path, "%s.%03d", filePath, chunk_count);
        printf("Chunkpath = %s\n", chunk_path);
        FILE *chunk_file = fopen(chunk_path, "wb");

        if (chunk_file == NULL) {
            fprintf(stderr, "Failed to create chunk file: %s\n", chunk_path);
            fclose(fp);
            return;
        }

        fwrite(read_file, 1, chunk_size, chunk_file);
        write_log("REPORT", "CREATE", chunk_path);
        fclose(chunk_file);

        chunk_count++;
    }

    fclose(fp);
    printf("Modularisasi selesai\n");
}


void modular_dir(const char* path){ 
    printf("ENTERING MODULAR\n");
    DIR *dir = opendir(path);
    if(dir == NULL){
        printf("Gagal membuka dir %s\n", path);
        return;
    }

    struct dirent *files;

    while ((files = readdir(dir)) != NULL) { 
            char file_path[1000];
            if(strcmp(files->d_name, ".") == 0 || strcmp(files->d_name, "..") == 0){
                continue;
            }
            sprintf(file_path, "%s/%s", path, files->d_name);
            printf("file : %s\n", file_path);
            struct stat st;
            if (stat(file_path, &st) == 0){
                printf("%ld\n", st.st_size);
                if(S_ISDIR(st.st_mode)) {
                    modular_dir(file_path);
                } else if(S_ISREG(st.st_mode) && st.st_size > CHUNK_SIZE){
                    printf("ENTERING CHUNK\n");
                    create_chunk_file(file_path);
                    unlink(file_path);
                    write_log("FLAG", "UNLINK", file_path);
                }
            }
    }

    closedir(dir);
    printf("Modularisasi direktori %s selesai\n", path);

}

void merge_modular(const char *path){
    printf("Entering merge\n");
  
        char cur[100];
        char filename[1000];

        char command[100];
        char basename[1000];
        char *merge_file_path = malloc(strlen(basename) + strlen(path) + 100);
        sprintf(command, "ls \"%s\" | sort", path);
        printf("com : %s\n", command);
        FILE *pipe = popen(command, "r");

        while (fscanf(pipe, "%s", filename) != EOF) {
            char *file_path = malloc(strlen(filename) + strlen(path) + 100);
            if(strcmp(filename, ".") == 0 || strcmp(filename, "..") == 0){
                continue;
            }

            sprintf(file_path, "%s/%s", path, filename);
            printf("File_path %s\n", file_path);

            struct stat st;
            if (stat(file_path, &st) == 0) {
            if (S_ISDIR(st.st_mode) && strstr(filename, "module_") == 0) {
                printf("%s is a directory.\n",filename);
                merge_modular(file_path);
            } else {
                printf("%s is a file.\n", filename);


            char* dot = strrchr(filename, '.');
            if (dot != NULL) {
              
                strncpy(basename, filename, dot - filename);
                basename[dot - filename] = '\0';

            
                strcpy(cur, dot + 1);

                
                printf("Basename: %s\n", basename);
                printf("Extension: %s\n", cur);
                if(atoi(cur) == 0 ){
                    sprintf(merge_file_path, "%s/%s", path, basename);
                    printf("merge : %s\n", merge_file_path);
                    write_log("REPORT", "CREATE",  merge_file_path);
                }
                char *command = malloc(strlen(file_path) + strlen(merge_file_path) + 15);
                sprintf(command, "cat \"%s\" >> \"%s\"", file_path, merge_file_path);
                system(command);
                unlink(file_path);
                write_log("FLAG", "UNLINK", file_path);
                } else {
                    printf("Invalid filename format.\n");
                }

                
            }
        }

    }

    }



static int xmp_rename(const char* old_path, const char* new_path){
    char *fullOld_path = malloc(strlen(dir_path) + strlen(old_path) + 3);
    sprintf(fullOld_path, "%s%s", dir_path, old_path);

    char *fullNew_path = malloc(strlen(dir_path) + strlen(new_path) + 3);
    sprintf(fullNew_path, "%s%s", dir_path, new_path);


    printf("New : %s\nOld : %s\n", new_path, old_path);
    if (strstr(new_path, "module_") != 0 && strstr(old_path, "module_") == 0) {
        printf("FULLOLD : %s\n", fullOld_path);
        modular_dir(fullOld_path);
    }

    else if(strstr(old_path, "module_") != 0 && strstr(new_path, "module_") == 0){
        merge_modular(fullOld_path);
    }

    int result = rename(fullOld_path, fullNew_path);
    if (result == -1) {
        return -errno;
    }

    printf("Start\n");
    char* report = malloc(strlen(fullOld_path) + strlen(fullNew_path) + 10);
    sprintf(report, "%s:%s", fullOld_path, fullNew_path);
    write_log("REPORT", "RENAME", report);
    printf("End\n");

    free(fullNew_path);
    free(fullOld_path);
    free(report);

    return 0;
}

static int xmp_mkdir(const char* path, mode_t mode) {
    int res;
    printf("xmp_mkdir called: %s\n", path);

    char *full_path = malloc(strlen(path) + strlen(dir_path) + 10);
    sprintf(full_path, "%s%s", dir_path, path);

    res = mkdir(full_path, mode);
    if (res == -1){
        return -errno;
    }

    write_log("REPORT", "MKDIR", path);

    return 0;
}

static int xmp_rmdir(const char *path) {	
    printf("xmp_rmdir called: %s\n", path);
	char newPath[1000];
	sprintf(newPath, "%s%s",dir_path, path);
	int result;
	result = rmdir(newPath);
    write_log("FLAG", "RMDIR", newPath);
	if (result == -1)
		return -errno;

	return 0;
}

void check_dir(const char* path) {
    printf("READDIR\n");
    write_log("REPORT", "READDIR", path);

    DIR *dp;
    struct dirent *de;
    dp = opendir(path);
    
    if (dp == NULL) {
        perror("Error opening directory");
        return;  
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0 || strcmp(de->d_name, "test") == 0) {
            continue;
        }
        printf("name : %s\n", de->d_name);

        char entry_path[1000];
        sprintf(entry_path, "%s/%s", path, de->d_name);

        char module_path[2000];
        if (strstr(de->d_name, "module_") != 0) {
            modular_dir(entry_path);
        }

        struct stat st;

        if(stat(entry_path, &st) == 0){
            if(S_ISDIR(st.st_mode)){
                printf("Entering subfolder\n");
                check_dir(entry_path);
            }
        }
    }

    closedir(dp);
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    if (!is_readdir_called) {
        check_dir(dir_path);
        is_readdir_called = 1;
    }
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev) {
    int res;

    if (S_ISREG(mode)) {
        res = creat(path, mode);
        if (res == -1)
            return -errno;
        close(res);
    } else if (S_ISFIFO(mode)) {
        res = mkfifo(path, mode);
        if (res == -1)
            return -errno;
    } else {
        res = mknod(path, mode, rdev);
        if (res == -1)
            return -errno;
    }


    write_log("REPORT", "MKNOD", path);

    return 0;
}

int xmp_access(const char *path, int mask) {
    int res;

    res = access(path, mask);
    if (res == -1)
        return -errno;

    write_log("REPORT", "ACCESS", path);

    return 0;
}

int xmp_readlink(const char *path, char *buf, size_t size) {
    int res;

    res = readlink(path, buf, size - 1);
    if (res == -1)
        return -errno;

    buf[res] = '\0';

    write_log("REPORT", "READLINK", path);

    return 0;
}

int xmp_statfs(const char *path, struct statvfs *stbuf) {
    int res;

    res = statvfs(path, stbuf);
    if (res == -1)
        return -errno;

    write_log("REPORT", "STATFS", path);

    return 0;
}

int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    int res;

    res = creat(path, mode);
    if (res == -1)
        return -errno;
    close(res);

    write_log("REPORT", "CREATE", path);

    return 0;
}

int xmp_release(const char *path, struct fuse_file_info *fi) {
    (void)path;
    (void)fi;

    write_log("REPORT", "RELEASE", path);

    return 0;
}

int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi) {
    (void)path;
    (void)isdatasync;
    (void)fi;

    write_log("REPORT", "FSYNC", path);

    return 0;
}

int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    int res;

    res = lsetxattr(path, name, value, size, flags);
    if (res == -1)
        return -errno;

    write_log("REPORT", "SETXATTR", path);

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

int xmp_listxattr(const char *path, char *list, size_t size) {
    int res;

    res = llistxattr(path, list, size);
    if (res == -1)
        return -errno;

    write_log("REPORT", "LISTXATTR", path);

    return res;
}

int xmp_removexattr(const char *path, const char *name) {
    int res;

    res = lremovexattr(path, name);
    if (res == -1)
        return -errno;

    write_log("REPORT", "REMOVEXATTR", path);

    return 0;
}



static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,

};


int main(int argc, char *argv[]){

    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
