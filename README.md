# sisop-praktikum-modul-4-2023-MH-IT13



## Anggota
1. Sylvia Febrianti - 5027221019
2. Zulfa Hafizh Kusuma - 5027221038
3. Muhammad Rifqi Oktaviansyah - 5027221067


## Soal 1
Anthoni Salim merupakan seorang pengusaha yang memiliki supermarket terbesar di Indonesia. Ia sedang melakukan inovasi besar dalam dunia bisnis ritelnya. Salah satu ide cemerlang yang sedang dia kembangkan adalah terkait dengan pengelolaan foto dan gambar produk dalam sistem manajemen bisnisnya. Anthoni bersama sejumlah rekan bisnisnya sedang membahas konsep baru yang akan mengubah cara produk-produk di supermarketnya dipresentasikan dalam katalog digital. Untuk resource dapat di download pada link ini.
- Pada folder “gallery”, agar katalog produk lebih menarik dan kreatif, Anthoni dan tim memutuskan untuk:
Membuat folder dengan prefix "rev." Dalam folder ini, setiap gambar yang dipindahkan ke dalamnya akan mengalami pembalikan nama file-nya. 
			Ex: "mv EBooVNhNe7tU7q08jgTe.HEIC rev-test/" 
Output: eTgj80q7Ut7eNhNVooBE.HEIC
Anthoni dan timnya ingin menghilangkan gambar-gambar produk yang sudah tidak lagi tersedia dengan membuat folder dengan prefix "delete." Jika sebuah gambar produk dipindahkan ke dalamnya, nama file-nya akan langsung terhapus.
Ex: "mv coba-deh.jpg delete-foto/" 
- Pada folder "sisop," terdapat file bernama "script.sh." Anthoni dan timnya menyadari pentingnya menjaga keamanan dan integritas data dalam folder ini. 
Mereka harus mengubah permission pada file "script.sh" karena jika dijalankan maka dapat menghapus semua dan isi dari  "gallery," "sisop," dan "tulisan."
Anthoni dan timnya juga ingin menambahkan fitur baru dengan membuat file dengan prefix "test" yang ketika disimpan akan mengalami pembalikan (reverse) isi dari file tersebut.  
- Pada folder "tulisan" Anthoni ingin meningkatkan kemampuan sistem mereka dalam mengelola berkas-berkas teks dengan menggunakan fuse.
Jika sebuah file memiliki prefix "base64," maka sistem akan langsung mendekode isi file tersebut dengan algoritma Base64.
Jika sebuah file memiliki prefix "rot13," maka isi file tersebut akan langsung di-decode dengan algoritma ROT13.
Jika sebuah file memiliki prefix "hex," maka isi file tersebut akan langsung di-decode dari representasi heksadesimalnya.
Jika sebuah file memiliki prefix "rev," maka isi file tersebut akan langsung di-decode dengan cara membalikkan teksnya.
- Pada folder “disable-area”, Anthoni dan timnya memutuskan untuk menerapkan kebijakan khusus. Mereka ingin memastikan bahwa folder dengan prefix "disable" tidak dapat diakses tanpa izin khusus. 
Jika seseorang ingin mengakses folder dan file pada “disable-area”, mereka harus memasukkan sebuah password terlebih dahulu (password bebas). 
- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
[SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
Ex:
[SUCCESS]::01/11/2023-10:43:43::rename::Move from /gallery/DuIJWColl2UYknZ8ubz6.HEIC to /gallery/foto/DuIJWColl2UYknZ8ubz6

## Penyelesaian
**hell.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fuse.h>

#define _FILE_OFFSET_BITS 64

static const char *dirpath = "/data/gallery";

static int hell_getattr(const char *path, struct stat *stbuf)
{
  if (strcmp(path, dirpath) == 0) {
    stbuf->st_mode = S_IFDIR | 0755;
    stbuf->st_nlink = 2;
    return 0;
  }

  return -ENOENT; // Declare ENOENT
}

static int hell_readlink(const char *path, char *buf, size_t size)
{
  if (strcmp(path, dirpath) == 0) {
    strcpy(buf, "rev-test");
    return strlen(buf);
  }

  return -ENOENT; // Declare ENOENT
}

static int hell_rename(const char *oldpath, const char *newpath)
{
  char *dir, *file;
  struct stat stbuf;

  if (strcmp(oldpath, dirpath) != 0 || strcmp(newpath, dirpath) == 0) {
    return -EINVAL; // Declare EINVAL
  }

  dir = strtok(oldpath, "/");
  file = strtok(NULL, "/");

  if (stat(dirpath, &stbuf) != 0) {
    return -ENOENT; // Declare ENOENT
  }

  if (!S_ISDIR(stbuf.st_mode)) {
    return -ENOTDIR; // Declare ENOTDIR
  }

  if (stat(oldpath, &stbuf) != 0) {
    return -ENOENT; // Declare ENOENT
  }

  if (!S_ISREG(stbuf.st_mode)) {
    return -EISDIR; // Declare EISDIR
  }

  if (strcmp(file, "rev-test") == 0) {
    char *newname = strrev(file); // Declared in string.h
    int ret = rename(oldpath, newpath);
    free(newname);
    return ret;
  }

  return -EINVAL; // Declare EINVAL
}

static struct fuse_operations hell_operations = {
  .getattr = hell_getattr,
  .readlink = hell_readlink, // Replace 'readdir' with 'readlink'
  .rename = hell_rename,
};

int main(int argc, char *argv[])
{
  return fuse_main(argc, argv, &hell_operations, NULL);
}
```

## Penjelasan Kode
```
static int hell_getattr(const char *path, struct stat *stbuf)
{
  if (strcmp(path, dirpath) == 0) {
    stbuf->st_mode = S_IFDIR | 0755;
    stbuf->st_nlink = 2;
    return 0;
  }

  return -ENOENT; // Declare ENOENT
}
```
Fungsi diatas berfungsi untuk mendapatkan informasi tentang file atau direktori. Fungsi ini menerima dua parameter: path dan stbuf. Path merupakan path ke file atau direktori yang ingin diakses sedangkan stbuf merupakan struktur `stat` yang akan diisi dengan informasi tentang file atau direktori.

```
static int hell_readlink(const char *path, char *buf, size_t size)
{
  if (strcmp(path, dirpath) == 0) {
    strcpy(buf, "rev-test");
    return strlen(buf);
  }

  return -ENOENT; // Declare ENOENT
}
```
Fungsi ini digunakan untuk membaca link. Fungsi ini menerima tiga parameter: path ke tautan simbolik yang ingin dibaca (path), buffer tempat tautan simbolik akan disimpan (buf), ukuran buffer (size). Fungsi ini mengembalikan jumlah byte yang berhasil dibaca, atau nilai negatif jika terjadi kesalahan.

```
static int hell_rename(const char *oldpath, const char *newpath)
{
  char *dir, *file;
  struct stat stbuf;

  if (strcmp(oldpath, dirpath) != 0 || strcmp(newpath, dirpath) == 0) {
    return -EINVAL; // Declare EINVAL
  }

  dir = strtok(oldpath, "/");
  file = strtok(NULL, "/");

  if (stat(dirpath, &stbuf) != 0) {
    return -ENOENT; // Declare ENOENT
  }

  if (!S_ISDIR(stbuf.st_mode)) {
    return -ENOTDIR; // Declare ENOTDIR
  }

  if (stat(oldpath, &stbuf) != 0) {
    return -ENOENT; // Declare ENOENT
  }

  if (!S_ISREG(stbuf.st_mode)) {
    return -EISDIR; // Declare EISDIR
  }

  if (strcmp(file, "rev-test") == 0) {
    char *newname = strrev(file); // Declared in string.h
    int ret = rename(oldpath, newpath);
    free(newname);
    return ret;
  }

  return -EINVAL; // Declare EINVAL
}
```
Fungsi diatas bertujuan untuk mengganti nama file atau direktori. Fungsi menerima parameter oldpath yaitu path lama file atau direketori dan newpath yaitu path baru file atau direktori. Fungsi ini pertama-tama memeriksa apakah path lama dan path baru sama. Jika sama, fungsi ini akan mengembalikan nilai -EINVAL. Selanjutnya, fungsi ini memecah path lama menjadi dua bagian: direktori induk dan nama file. Fungsi ini menggunakan fungsi strtok() untuk melakukan pemecahan. Fungsi ini kemudian memeriksa apakah direktori induk path lama ada. Jika tidak ada, fungsi ini akan mengembalikan nilai -ENOENT. Fungsi ini kemudian memeriksa apakah direktori induk path baru ada. Jika tidak ada, fungsi ini akan membuat direktori induk tersebut. Fungsi ini kemudian memeriksa apakah file atau direktori di path lama ada. Jika tidak ada, fungsi ini akan mengembalikan nilai -ENOENT. Fungsi ini kemudian memeriksa apakah file atau direktori di path baru ada. Jika ada, dan path baru adalah direktori, fungsi ini akan mengembalikan nilai -EISDIR. Jika semua pemeriksaan di atas berhasil, fungsi ini akan memanggil fungsi strrev() untuk membalik nama file. Fungsi strrev() dideklarasikan di header file string.h. Setelah nama file dibalik, fungsi ini akan memanggil fungsi rename() untuk mengganti nama file atau direktori. Fungsi rename() mengembalikan nilai 0 jika berhasil, dan nilai negatif jika terjadi kesalahan. Jika fungsi rename() mengembalikan nilai negatif, fungsi hell_rename() akan mengembalikan nilai yang sama.

```
static struct fuse_operations hell_operations = {
  .getattr = hell_getattr,
  .readlink = hell_readlink, // Replace 'readdir' with 'readlink'
  .rename = hell_rename,
};
```
Fungsi diatas digunakan untuk mendefinisikan struktur statis dari tipe struct fuse_operations bernama hell_operations. Struktur ini digunakan untuk menyimpan fungsi balik (callback function) yang akan dipanggil oleh pustaka FUSE untuk menangani berbagai operasi sistem file. Fungsi diatas memiliki beberapa anggota struktur. Fungsi getattr dipanggil oleh pustaka untuk mendapatkan informasi tentang file atau direktori. Fungsi readlink berguna untuk membaca link. Sedangkan fungsi rename berguna untuk mengganti nama file atau direktori.

```
int main(int argc, char *argv[])
{
  return fuse_main(argc, argv, &hell_operations, NULL);
}
```
Fungsi main diatas berguna untuk memulai sistem FUSE. Fungsi ini mengambil dua parameter: jumlah argumen baris perintah dan array argumen baris perintah.

## Kendala 
Program memiliki kendala dimana program selalu menampilkan error pada saat di compile seperti terlampir pada sub-bab screenshot.

## Screenshot
- Error saat compiling
![Error Compiling](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_1/error.png)

## Soal 2
Manda adalah seorang mahasiswa IT, dimana ia merasa hari ini adalah hari yang menyebalkan karena sudah bertemu lagi dengan praktikum sistem operasi. Pada materi hari ini, ia mempelajari suatu hal bernama FUSE. Karena sesi lab telah selesai, kini waktunya untuk mengerjakan penugasan praktikum. Manda mendapatkan firasat jika soal modul kali ini adalah yang paling sulit dibandingkan modul lainnya, sehingga dia akan mulai mengerjakan tugas praktikum-nya. Sebelumnya, Manda mendapatkan file yang perlu didownload secara manual terlebih dahulu pada link ini. Selanjutnya, ia tinggal mengikuti langkah - langkah yang diminta untuk mengerjakan soal ini hingga akhir. Manda berharap ini menjadi modul terakhir sisop yang ia pelajari

**Membuat file open-password.c untuk membaca file zip-pass.txt**
```
- Melakukan proses dekripsi base64 terhadap file tersebut
- Lalu unzip home.zip menggunakan password hasil dekripsi
```
**Membuat file semangat.c**
```
- Setiap proses yang dilakukan akan tercatat pada logs-fuse.log dengan format :
- [SUCCESS/FAILED]::dd/mm/yyyy-hh:mm:ss::[tag]::[information]
  Ex:
  [SUCCESS]::06/11/2023-16:05:48::readdir::Read directory /sisop
- Selanjutnya untuk memudahkan pengelolaan file, Manda diminta untuk mengklasifikasikan file - 
  sesuai jenisnya:
- Membuat folder yang dapat langsung memindahkan file dengan kategori ekstensi file yang mereka 
  tetapkan:
  documents: .pdf dan .docx. 
  images: .jpg, .png, dan .ico. 
  website: .js, .html, dan .json. 
  sisop: .c dan .sh. 
  text: .txt. 
  aI: .ipynb dan .csv.
- Karena sedang belajar tentang keamanan, tiap kali mengakses file (cat nama-file) pada di 
  dalam folder text maka perlu memasukkan password terlebih dahulu
- Password terdapat di file password.bin
- Pada folder website
  Membuat file csv pada folder website dengan format ini: file,title,body
- Tiap kali membaca file csv dengan format yang telah ditentukan, maka akan langsung 
  tergenerate sebuah file html sejumlah yang telah dibuat pada file csv
- Tidak dapat menghapus file / folder yang mengandung prefix “restricted”
- Pada folder documents
  Karena ingin mencatat hal - hal penting yang mungkin diperlukan, maka Manda diminta untuk menambahkan detail - detail kecil dengan memanfaatkan attribut
```
**Membuat file server.c**
```
- Pada folder ai, terdapat file webtoon.csv
- Manda diminta untuk membuat sebuah server (socket programming) untuk membaca webtoon.csv.   
- Dimana terjadi pengiriman data antara client ke server dan server ke client.
  Menampilkan seluruh judul
  Menampilkan berdasarkan genre
  Menampilkan berdasarkan hari
  Menambahkan ke dalam file webtoon.csv
  Melakukan delete berdasarkan judul
  Selain command yang diberikan akan menampilkan tulisan “Invalid Command”
  Manfaatkan client.c pada folder sisop sebagai client
```
## Penyelesaian
**open-password.c**
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


char *base64decoder(char encoded[], int string_length) {
    char *decoded_string = (char *)malloc(sizeof(char) * 100);

    int i, j, k = 0;
    int num = 0;
    int count_bits = 0;

    for (i = 0; i < string_length; i += 4) {
        num = 0, count_bits = 0;

        for (j = 0; j < 4; j++) {
            if (encoded[i + j] != '=') {
                num = num << 6;
                count_bits += 6;
            }

            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);
            else if (encoded[i + j] == '+')
                num = num | 62;
            else if (encoded[i + j] == '/')
                num = num | 63;
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0) {
            count_bits -= 8;
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    decoded_string[k] = '\0';

    return decoded_string;
}

int main() {
    char encoded[] = "RiNhNHhQWlMuLGpmZ1VrI25UQGw=";

    int string_length = strlen(encoded);

    char *decoded_string = base64decoder(encoded, string_length);

    printf("Decoded String: %s\n", decoded_string);

    free(decoded_string);

    return 0;
}

```

**semangat.c**
```
#define FUSE_USE_VERSION 28
#include <stdbool.h>
#include <sys/stat.h>
#include <utime.h>
#include <time.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>

static const char *dirpath = "/home/user/lastmod_soal2/home";

static FILE *log_file;

// Open log file when the filesystem is mounted
static void open_log_file() {
    char log_path[1000];
    sprintf(log_path, "%s/logs-fuse.log", dirpath);
    log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Error opening log file");
        exit(EXIT_FAILURE);
    }
}

// Log an operation
static void log_operation(const char *operation, const char *path) {
    time_t t;
    struct tm *tm_info;
    char timestamp[20];

    time(&t);
    tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    fprintf(log_file, "[%s] %s: %s\n", timestamp, operation, path);
    fflush(log_file);
}

static int xmp_mkdir(const char *path, mode_t mode) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;

    log_operation("MKDIR", path);

    return 0;
}

static int xmp_rmdir(const char *path) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = rmdir(fpath);
    if (res == -1)
        return -errno;

    log_operation("RMDIR", path);

    return 0;
}

static bool is_extension(const char *filename, const char *ext) {
    const char *dot = strrchr(filename, '.');
    return dot && strcmp(dot + 1, ext) == 0;
}


static int move_file(const char *src_path, const char *dest_path) {
    if (rename(src_path, dest_path) == 0) {
        return 0; // Success
    } else {
        perror("Error moving file");
        return -1;
    }
}


static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = creat(fpath, mode);
    if (fd == -1)
        return -errno;

    fi->fh = fd;

    log_operation("CREATE", path);

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;

    log_operation("OPEN", path);
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = pwrite(fi->fh, buf, size, offset);
    if (res == -1)
        res = -errno;

    log_operation("WRITE", path);
    return res;
}

static int xmp_unlink(const char *path) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = unlink(fpath);
    if (res == -1)
        return -errno;

    log_operation("UNLINK", path);
    return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2]) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = utimensat(0, fpath, ts, AT_SYMLINK_NOFOLLOW);
    if (res == -1)
        return -errno;

    log_operation("UTIMENS", path);

    return 0;
}

static int xmp_truncate(const char *path, off_t size) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = truncate(fpath, size);
    if (res == -1)
        return -errno;

    log_operation("TRUNCATE", path);

    return 0;
}

static int xmp_flush(const char *path, struct fuse_file_info *fi) {
    (void) path;
    int res = close(fi->fh);
    if (res == -1)
        return -errno;

    log_operation("FLUSH", path);

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf) {
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    log_operation("GET ATTRIBUTE", path);

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];

    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char full_path[4096];
        sprintf(full_path, "%s/%s", fpath, de->d_name);

        // Check if this is a regular file
        if (S_ISREG(st.st_mode)) {
            // Implementation of file movement based on folders here
            if (is_extension(de->d_name, "pdf") || is_extension(de->d_name, "docx")) {
                xmp_mkdir("/documents", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/documents/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "jpg") || is_extension(de->d_name, "png") || is_extension(de->d_name, "ico")) {
                xmp_mkdir("/images", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/images/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "js") || is_extension(de->d_name, "html") || is_extension(de->d_name, "json")) {
                xmp_mkdir("/website", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/website/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "c") || is_extension(de->d_name, "sh")) {
                xmp_mkdir("/sisop", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/sisop/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "txt")) {
                xmp_mkdir("/text", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/text/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "ipynb") || is_extension(de->d_name, "csv")) {
                xmp_mkdir("/aI", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/aI/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            }
        }
        res = (filler(buf, de->d_name, &st, 0));

        if (res != 0)
            break;
    }

    closedir(dp);

    log_operation("READ DIRECTORY", path);

    return 0;
}

static int xmp_chmod(const char *path, mode_t mode) {
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = chmod(fpath, mode);
    if (res == -1)
        return -errno;

    log_operation("CHMOD", path);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else {
        sprintf(fpath, "%s%s", dirpath, path);
    }

    int res = 0;

    (void)fi;

    int fd = open(fpath, O_RDONLY);

    if (fd == -1)
        return -errno;

    // Check if the file is in the "text" folder
    if (strstr(fpath, "/text/") != NULL) {
        // File is in the "text" folder, prompt for password
        char password[100];
        printf("Enter the password for file %s: ", path);
        scanf("%99s", password);

        // Compare the entered password with the stored password in "password.bin"
        char password_path[1000];
        sprintf(password_path, "%s/password.bin", dirpath);

        FILE *password_file = fopen(password_path, "r");
        if (password_file == NULL) {
            perror("Error opening password file");
            close(fd);
            return -errno;
        }

        char stored_password[100];
        fscanf(password_file, "%99s", stored_password);
        fclose(password_file);

        if (strcmp(password, stored_password) != 0) {
            printf("Incorrect password\n");
            close(fd);
            return -EACCES;  // Access denied
        }
    }

    res = pread(fd, buf, size, offset);

    if (res == -1)
        res = -errno;

    close(fd);

    log_operation("READ", path);
    return res;
}
// Cleanup function called when the filesystem is unmounted
static void xmp_destroy(void *userdata) {
    (void) userdata; // unused parameter

    fclose(log_file); // Close the log file
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .create = xmp_create,
    .open = xmp_open,
    .write = xmp_write,
    .unlink = xmp_unlink,
    .utimens = xmp_utimens,
    .truncate = xmp_truncate,
    .flush = xmp_flush,
    .chmod = xmp_chmod,
    .destroy = xmp_destroy,
};

int main(int argc, char *argv[]) {
    umask(0);

    open_log_file();

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

## Penjelasan kode
**open-password.c**
```
char *decoded_string = (char *)malloc(sizeof(char) * 100);
```
Membuat array dinamis untuk menyimpan hasil dekode. Pengalokasian memori dilakukan dengan fungsi `malloc`.

```
for (i = 0; i < string_length; i += 4) {
            num = 0, count_bits = 0;

            for (j = 0; j < 4; j++) {
                // ...
            }

            while (count_bits != 0) {
                // ...
            }
        }
        ```

```
- Di dalam iterasi pertama, nilai-nilai `num` dan `count_bits` diatur ulang untuk setiap blok empat karakter.
- Iterasi kedua adalah untuk setiap karakter dalam blok empat karakter tersebut
- Setiap karakter dalam blok diolah untuk mengonversi karakter Base64 ke nilai numerik (0-63).
- Iterasi ketiga mengonversi nilai numerik ke dalam bentuk biner (menggunakan operasi bitwise) 
  dan menempatkannya dalam string hasil dekode
- decoded_string[k] = '\0';`: Menambahkan karakter null pada akhir string hasil dekode.
- `return decoded_string;`: Mengembalikan hasil dekode.

```
int main() {
        char encoded[] = "RiNhNHhQWlMuLGpmZ1VrI25UQGw=";

        int string_length = strlen(encoded);

        char *decoded_string = base64decoder(encoded, string_length);

        printf("Decoded String: %s\n", decoded_string);

        free(decoded_string);

        return 0;
    }
```
- char encoded[] = "RiNhNHhQWlMuLGpmZ1VrI25UQGw=";` : Mendefinisikan string terenkripsi yang akan di-dekode.
- int string_length = strlen(encoded); : Menghitung panjang string terenkripsi.
- Memanggil fungsi `base64decoder` untuk melakukan dekode dan mencetak hasilnya.
- free(decoded_string); : Membebaskan memori yang telah dialokasikan untuk hasil dekode setelah tidak digunakan lagi.

**semangat.c**
```
open_log_file 
```
- Fungsi ini membuka file log saat sistem berkas dimount. File log tersebut digunakan untuk 
  mencatat operasi-operasi yang dilakukan pada sistem berkas.

```
log_operation 
```
- Fungsi ini mencatat operasi yang dilakukan pada sistem berkas ke dalam file log beserta 
  timestamp dan path yang terkait.

```
xmp_mkdir
``` 
- Fungsi ini menggantikan fungsi mkdir pada sistem berkas standar dan mencatat operasi 
  pembuatan direktori ke dalam file log.

```
xmp_rmdir 
```
- Fungsi ini menggantikan fungsi rmdir pada sistem berkas standar dan mencatat operasi 
  penghapusan direktori ke dalam file log.

```
is_extension 
```
- Fungsi ini memeriksa apakah sebuah nama file memiliki ekstensi tertentu.

```
move_file 
```
- Fungsi ini memindahkan file dari satu lokasi ke lokasi lainnya.

```
xmp_create 
```
- Fungsi ini menggantikan fungsi creat pada sistem berkas standar dan mencatat operasi 
  pembuatan file ke dalam file log.

```
xmp_open
```
- Fungsi ini menggantikan fungsi open pada sistem berkas standar dan mencatat operasi membuka 
  file ke dalam file log.

```
xmp_write
```
- Fungsi ini menggantikan fungsi write pada sistem berkas standar dan mencatat operasi 
  penulisan ke dalam file log.

```
xmp_unlink
``` 
- Fungsi ini menggantikan fungsi unlink pada sistem berkas standar dan mencatat operasi 
  penghapusan file ke dalam file log.

```
xmp_utimens 
```
- Fungsi ini menggantikan fungsi utimensat pada sistem berkas standar dan mencatat operasi 
  perubahan waktu akses dan modifikasi ke dalam file log.

```
xmp_truncate
```
- Fungsi ini menggantikan fungsi truncate pada sistem berkas standar dan mencatat operasi 
  perubahan ukuran file ke dalam file log.

```
xmp_flush 
```
- Fungsi ini menggantikan fungsi close pada sistem berkas standar dan mencatat operasi 
  penulisan ke dalam file log.

```
xmp_getattr 
```
- Fungsi ini menggantikan fungsi lstat pada sistem berkas standar dan mencatat operasi 
  mendapatkan atribut file ke dalam file log.

```
xmp_readdir
``` 
- Fungsi ini menggantikan fungsi readdir pada sistem berkas standar dan mencatat operasi 
  membaca direktori ke dalam file log.

```
xmp_chmod
```
- Fungsi ini menggantikan fungsi chmod pada sistem berkas standar dan mencatat operasi 
  perubahan mode (permission) ke dalam file log.

```
xmp_read
```
- Fungsi ini menggantikan fungsi pread pada sistem berkas standar dan mencatat operasi membaca 
  file ke dalam file log. Juga, melakukan verifikasi password untuk membaca file dalam direktori "text".

```
xmp_destroy
```
- Fungsi ini dipanggil saat sistem berkas di-unmount dan digunakan untuk membersihkan sumber 
  daya, dalam hal ini menutup file log.

```
xmp_oper
```
- Struktur ini berisi pointer ke fungsi-fungsi yang mengimplementasikan operasi-operasi pada
  sistem berkas FUSE.

```
main
```
- Fungsi utama program yang melakukan inisialisasi dan memanggil fuse_main untuk menjalankan sistem berkas FUSE.

## Kendala
program ini belum selesai. Masih ditahap mengelompokkan file berdasarkan extensionnya.

## Screenshots
- open-password.c
![open-password](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_2/open-password.png)
- kategori file
![kelompok file](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_2/kelompok_file.png)
- logs-fuse
![logs-fuse](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_2/logs-fuse.png)
- Error saat cat file txt
![Error cat](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_2/error_cat_txt.png)

## Soal 3
Dalam kegelapan malam yang sepi, bulan purnama menerangi langit dengan cahaya peraknya. Rumah-rumah di sekitar daerah itu terlihat seperti bayangan yang menyelimuti jalan-jalan kecil yang sepi. Suasana ini terasa aneh, membuat hati seorang wanita bernama Sarah merasa tidak nyaman.<br>
Dia berjalan ke jendela kamarnya, menatap keluar ke halaman belakang. Pohon-pohon besar di halaman tampak gelap dan menyeramkan dalam sinar bulan. Sesekali, cahaya bulan yang redup itu memperlihatkan bayangan-bayangan aneh yang bergerak di antara pepohonan.<br>
Tiba-tiba, Ting! Pandangannya tertuju pada layar smartphonenya. Dia terkejut bukan kepalang. Notifikasi barusan adalah remainder pengumpulan praktikum yang dikirim oleh asisten praktikumnya. Sarah lupa bahwa 2 jam lagi adalah waktu pengumpulan tugas praktikum mata kuliah Sistem Operasi. Sarah sejenak merasa putus asa, saat dia menyadari bahwa ketidaknyamanan yang dirasakannya adalah akibat tekanan tugas praktikumnya.<br>
Sarah melihat tugas praktikum yang harus dia selesaikan, dan dalam hitungan detik, rasa panik melanda. Tugas ini tampaknya sangat kompleks, dan dia belum sepenuhnya siap. Sebagai seorang mahasiswa teknik komputer, mata kuliah Sistem Operasi adalah salah satu mata kuliah yang sangat menuntut, dan dia harus menyelesaikan tugas ini dengan baik untuk menjaga nilai akademiknya.<br>
Tugas yang diberikan adalah untuk membuat sebuah filesystem dengan ketentuan sebagai berikut:
- Pada filesystem tersebut, jika User membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 
- Jika sebuah direktori adalah direktori modular, maka akan dilakukan modularisasi pada folder tersebut dan juga sub-direktorinya.
- Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan berikut:
Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.
Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.
Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

```
Contoh:
REPORT::231105-12:29:28::RENAME::/home/sarah/selfie.jpg::/home/sarah/cantik.jpg
REPORT::231105-12:29:33::CREATE::/home/sarah/test.txt
FLAG::231105-12:29:33::RMDIR::/home/sarah/folder
```

- Saat dilakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Sarah.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Sarah.txt.000, File_Sarah.txt.001, dan File_Sarah.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).
- Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).
```
Contoh :
/tmp/test_fuse/ adalah filesystem yang harus dirancang.
/home/index/modular/ adalah direktori asli.
```

## Penyelesaian
**easy.c**
```
#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>

#define CHUNK_SIZE 1024

static const char * dir_path = "/home/syl/modular";
static int is_readdir_called = 0;

void write_log(const char* flag, const char* cmd, const char* desc) {
    time_t now = time(NULL);
    struct tm* tm_info = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", tm_info);

    FILE* log_file = fopen("/home/syl/fs_module.log", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        return;
    }

    fprintf(log_file, "%s::%s::%s::%s\n", flag, timestamp, cmd, desc);
    fclose(log_file);
}


static int xmp_getattr(const char *path, struct stat *stbuf){
    int res;
    char full_path[256];

    sprintf(full_path, "%s%s", dir_path, path);

    res = lstat(full_path, stbuf);
    if (res == -1)
        return -errno;

    return 0;
}

void create_chunk_file(const char *filePath){
    printf("Filepath %s\n", filePath);

    FILE* fp = fopen(filePath, "rb");

    if(fp == NULL){
        printf("Gagal membuka file %s\n", filePath);
        exit(EXIT_FAILURE);
    }

    char chunk_path[256];
    int chunk_count = 0;
    char read_file[CHUNK_SIZE];

    size_t chunk_size;
    while((chunk_size = fread(read_file, 1, CHUNK_SIZE, fp)) > 0){
        sprintf(chunk_path, "%s.%03d", filePath, chunk_count);
        printf("Chunkpath = %s\n", chunk_path);
        FILE *chunk_file = fopen(chunk_path, "wb");

        if (chunk_file == NULL) {
            fprintf(stderr, "Failed to create chunk file: %s\n", chunk_path);
            fclose(fp);
            return;
        }

        fwrite(read_file, 1, chunk_size, chunk_file);
        write_log("REPORT", "CREATE", chunk_path);
        fclose(chunk_file);

        chunk_count++;
    }

    fclose(fp);
    printf("Modularisasi selesai\n");
}


void modular_dir(const char* path){ 
    printf("ENTERING MODULAR\n");
    DIR *dir = opendir(path);
    if(dir == NULL){
        printf("Gagal membuka dir %s\n", path);
        return;
    }

    struct dirent *files;

    while ((files = readdir(dir)) != NULL) { 
            char file_path[1000];
            if(strcmp(files->d_name, ".") == 0 || strcmp(files->d_name, "..") == 0){
                continue;
            }
            sprintf(file_path, "%s/%s", path, files->d_name);
            printf("file : %s\n", file_path);
            struct stat st;
            if (stat(file_path, &st) == 0){
                printf("%ld\n", st.st_size);
                if(S_ISDIR(st.st_mode)) {
                    modular_dir(file_path);
                } else if(S_ISREG(st.st_mode) && st.st_size > CHUNK_SIZE){
                    printf("ENTERING CHUNK\n");
                    create_chunk_file(file_path);
                    unlink(file_path);
                    write_log("FLAG", "UNLINK", file_path);
                }
            }
    }

    closedir(dir);
    printf("Modularisasi direktori %s selesai\n", path);

}

void merge_modular(const char *path){
    printf("Entering merge\n");
  
        char cur[100];
        char filename[1000];

        char command[100];
        char basename[1000];
        char *merge_file_path = malloc(strlen(basename) + strlen(path) + 100);
        sprintf(command, "ls \"%s\" | sort", path);
        printf("com : %s\n", command);
        FILE *pipe = popen(command, "r");

        while (fscanf(pipe, "%s", filename) != EOF) {
            char *file_path = malloc(strlen(filename) + strlen(path) + 100);
            if(strcmp(filename, ".") == 0 || strcmp(filename, "..") == 0){
                continue;
            }

            sprintf(file_path, "%s/%s", path, filename);
            printf("File_path %s\n", file_path);

            struct stat st;
            if (stat(file_path, &st) == 0) {
            if (S_ISDIR(st.st_mode) && strstr(filename, "module_") == 0) {
                printf("%s is a directory.\n",filename);
                merge_modular(file_path);
            } else {
                printf("%s is a file.\n", filename);


            char* dot = strrchr(filename, '.');
            if (dot != NULL) {
              
                strncpy(basename, filename, dot - filename);
                basename[dot - filename] = '\0';

            
                strcpy(cur, dot + 1);

                
                printf("Basename: %s\n", basename);
                printf("Extension: %s\n", cur);
                if(atoi(cur) == 0 ){
                    sprintf(merge_file_path, "%s/%s", path, basename);
                    printf("merge : %s\n", merge_file_path);
                    write_log("REPORT", "CREATE",  merge_file_path);
                }
                char *command = malloc(strlen(file_path) + strlen(merge_file_path) + 15);
                sprintf(command, "cat \"%s\" >> \"%s\"", file_path, merge_file_path);
                system(command);
                unlink(file_path);
                write_log("FLAG", "UNLINK", file_path);
                } else {
                    printf("Invalid filename format.\n");
                }

                
            }
        }

    }

    }



static int xmp_rename(const char* old_path, const char* new_path){
    char *fullOld_path = malloc(strlen(dir_path) + strlen(old_path) + 3);
    sprintf(fullOld_path, "%s%s", dir_path, old_path);

    char *fullNew_path = malloc(strlen(dir_path) + strlen(new_path) + 3);
    sprintf(fullNew_path, "%s%s", dir_path, new_path);


    printf("New : %s\nOld : %s\n", new_path, old_path);
    if (strstr(new_path, "module_") != 0 && strstr(old_path, "module_") == 0) {
        printf("FULLOLD : %s\n", fullOld_path);
        modular_dir(fullOld_path);
    }

    else if(strstr(old_path, "module_") != 0 && strstr(new_path, "module_") == 0){
        merge_modular(fullOld_path);
    }

    int result = rename(fullOld_path, fullNew_path);
    if (result == -1) {
        return -errno;
    }

    printf("Start\n");
    char* report = malloc(strlen(fullOld_path) + strlen(fullNew_path) + 10);
    sprintf(report, "%s:%s", fullOld_path, fullNew_path);
    write_log("REPORT", "RENAME", report);
    printf("End\n");

    free(fullNew_path);
    free(fullOld_path);
    free(report);

    return 0;
}

static int xmp_mkdir(const char* path, mode_t mode) {
    int res;
    printf("xmp_mkdir called: %s\n", path);

    char *full_path = malloc(strlen(path) + strlen(dir_path) + 10);
    sprintf(full_path, "%s%s", dir_path, path);

    res = mkdir(full_path, mode);
    if (res == -1){
        return -errno;
    }

    write_log("REPORT", "MKDIR", path);

    return 0;
}

static int xmp_rmdir(const char *path) {	
    printf("xmp_rmdir called: %s\n", path);
	char newPath[1000];
	sprintf(newPath, "%s%s",dir_path, path);
	int result;
	result = rmdir(newPath);
    write_log("FLAG", "RMDIR", newPath);
	if (result == -1)
		return -errno;

	return 0;
}

void check_dir(const char* path) {
    printf("READDIR\n");
    write_log("REPORT", "READDIR", path);

    DIR *dp;
    struct dirent *de;
    dp = opendir(path);
    
    if (dp == NULL) {
        perror("Error opening directory");
        return;  
    }

    while ((de = readdir(dp)) != NULL) {
        if (strcmp(de->d_name, ".") == 0 || strcmp(de->d_name, "..") == 0 || strcmp(de->d_name, "test") == 0) {
            continue;
        }
        printf("name : %s\n", de->d_name);

        char entry_path[1000];
        sprintf(entry_path, "%s/%s", path, de->d_name);

        char module_path[2000];
        if (strstr(de->d_name, "module_") != 0) {
            modular_dir(entry_path);
        }

        struct stat st;

        if(stat(entry_path, &st) == 0){
            if(S_ISDIR(st.st_mode)){
                printf("Entering subfolder\n");
                check_dir(entry_path);
            }
        }
    }

    closedir(dp);
}


static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    if (!is_readdir_called) {
        check_dir(dir_path);
        is_readdir_called = 1;
    }
    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev) {
    int res;

    if (S_ISREG(mode)) {
        res = creat(path, mode);
        if (res == -1)
            return -errno;
        close(res);
    } else if (S_ISFIFO(mode)) {
        res = mkfifo(path, mode);
        if (res == -1)
            return -errno;
    } else {
        res = mknod(path, mode, rdev);
        if (res == -1)
            return -errno;
    }


    write_log("REPORT", "MKNOD", path);

    return 0;
}

int xmp_access(const char *path, int mask) {
    int res;

    res = access(path, mask);
    if (res == -1)
        return -errno;

    write_log("REPORT", "ACCESS", path);

    return 0;
}

int xmp_readlink(const char *path, char *buf, size_t size) {
    int res;

    res = readlink(path, buf, size - 1);
    if (res == -1)
        return -errno;

    buf[res] = '\0';

    write_log("REPORT", "READLINK", path);

    return 0;
}

int xmp_statfs(const char *path, struct statvfs *stbuf) {
    int res;

    res = statvfs(path, stbuf);
    if (res == -1)
        return -errno;

    write_log("REPORT", "STATFS", path);

    return 0;
}

int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    int res;

    res = creat(path, mode);
    if (res == -1)
        return -errno;
    close(res);

    write_log("REPORT", "CREATE", path);

    return 0;
}

int xmp_release(const char *path, struct fuse_file_info *fi) {
    (void)path;
    (void)fi;

    write_log("REPORT", "RELEASE", path);

    return 0;
}

int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi) {
    (void)path;
    (void)isdatasync;
    (void)fi;

    write_log("REPORT", "FSYNC", path);

    return 0;
}

int xmp_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
    int res;

    res = lsetxattr(path, name, value, size, flags);
    if (res == -1)
        return -errno;

    write_log("REPORT", "SETXATTR", path);

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
    {
        return -errno;
    }

    return 0;
}

int xmp_listxattr(const char *path, char *list, size_t size) {
    int res;

    res = llistxattr(path, list, size);
    if (res == -1)
        return -errno;

    write_log("REPORT", "LISTXATTR", path);

    return res;
}

int xmp_removexattr(const char *path, const char *name) {
    int res;

    res = lremovexattr(path, name);
    if (res == -1)
        return -errno;

    write_log("REPORT", "REMOVEXATTR", path);

    return 0;
}



static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .access = xmp_access,
    .readlink = xmp_readlink,
    .readdir = xmp_readdir,
    .mknod = xmp_mknod,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .rename = xmp_rename,
    .statfs = xmp_statfs,
    .create = xmp_create,
    .release = xmp_release,
    .fsync = xmp_fsync,
    .setxattr = xmp_setxattr,
    .getxattr = xmp_getxattr,
    .listxattr = xmp_listxattr,
    .removexattr = xmp_removexattr,

};


int main(int argc, char *argv[]){

    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```

## Penjelasan Code
```
#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <attr/xattr.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
```
- Baris pertama (#define FUSE_USE_VERSION 28) mendefinisikan versi FUSE yang akan digunakan.
- Berikutnya adalah beberapa header file yang diperlukan untuk operasi FUSE dan fungsi sistem operasi lainnya.
- dir_path adalah direktori yang akan digunakan oleh sistem file ini.
- is_readdir_called adalah variabel yang menandakan apakah fungsi readdir sudah dipanggil atau belum.
```
void write_log(const char* flag, const char* cmd, const char* desc) {
    // ...
}
```
Fungsi ini bertujuan untuk mencatat kegiatan dalam file log. Parameter flag adalah tanda untuk jenis log, cmd adalah perintah atau aktivitas yang dicatat, dan desc adalah deskripsi terkait aktivitas tersebut.
```
static int xmp_getattr(const char *path, struct stat *stbuf) {
    // ...
}
```
Fungsi ini digunakan untuk mengambil informasi atribut dari file atau direktori yang diberikan oleh path. Informasi ini kemudian disimpan dalam struktur stbuf yang berisi informasi seperti ukuran file, waktu pembuatan, dan hak akses.
```
void create_chunk_file(const char *filePath) {
    // ...
}
```
Fungsi ini membagi file yang besar menjadi potongan-potongan kecil (CHUNK_SIZE) dan membuat file-file kecil berdasarkan potongan tersebut. Ini dilakukan dengan membaca file asli dan membuat potongan-potongan yang disimpan dalam file-file terpisah.
```
void modular_dir(const char* path) {
    // ...
}
```
Fungsi ini bertanggung jawab untuk memproses direktori yang diberikan. Jika sebuah file di dalamnya cukup besar, maka akan dilakukan proses modularisasi dengan memanggil fungsi create_chunk_file.
```
void merge_modular(const char *path) {
    // ...
}
```
Fungsi ini menggabungkan kembali potongan-potongan file menjadi satu file utuh berdasarkan aturan tertentu. Ini dilakukan dengan membaca potongan-potongan file dan menggabungkannya kembali menjadi satu file yang utuh.<br>
<br>
Ada beberapa fungsi lain seperti xmp_rename, xmp_mkdir, xmp_rmdir, xmp_readdir, xmp_create, xmp_release, dan sebagainya yang mengimplementasikan operasi dasar sistem file seperti rename, membuat direktori, menghapus direktori, membaca direktori, membuat file, dan melepaskan file.
```
int main(int argc, char *argv[]) {
    // ...
}
```
Fungsi main inilah yang akan dijalankan pertama kali saat program ini dieksekusi. Ini akan memulai FUSE dan memasukkan semua fungsi yang telah didefinisikan ke dalam struktur xmp_oper.

## Screenshot
- Direktori tujuan
![Direktori tujuan](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_3/dirtujuan.png)

- Direktori asal
![Direktori asal](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_3/dirasal.png)

- Isi file
![cat](https://gitlab.com/sylxer/sisop-praktikum-modul-4-2023-mh-it13/-/raw/main/Screenshoot/soal_3/isi.png)
