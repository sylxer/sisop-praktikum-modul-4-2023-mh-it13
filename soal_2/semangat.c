#define FUSE_USE_VERSION 28
#include <stdbool.h>
#include <sys/stat.h>
#include <utime.h>
#include <time.h>
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <stdlib.h>

static  const  char *dirpath = "/home/user/lastmod_soal2/home";

static FILE *log_file;

static void open_log_file() {
    char log_path[1000];
    sprintf(log_path, "%s/logs-fuse.log", dirpath);
    log_file = fopen(log_path, "a");
    if (log_file == NULL) {
        perror("Error opening log file");
        exit(EXIT_FAILURE);
    }
}

// Log an operation
static void log_operation(const char *operation, const char *path) {
    time_t t;
    struct tm *tm_info;
    char timestamp[20];

    time(&t);
    tm_info = localtime(&t);

    strftime(timestamp, sizeof(timestamp), "%Y-%m-%d %H:%M:%S", tm_info);

    fprintf(log_file, "[%s] %s: %s\n", timestamp, operation, path);
    fflush(log_file);
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_rmdir(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = rmdir(fpath);
    if (res == -1)
        return -errno;

    return 0;
}

static bool is_extension(const char *filename, const char *ext) {
    const char *dot = strrchr(filename, '.');
    return dot && strcmp(dot + 1, ext) == 0;
}

static int move_file(const char *src_path, const char *dest_path) {
    int src_fd = open(src_path, O_RDONLY);
    if (src_fd == -1) {
        perror("Error opening source file");
        return -1;
    }

    int dest_fd = creat(dest_path, S_IRWXU | S_IRGRP | S_IROTH); // Adjust permission as needed
    if (dest_fd == -1) {
        perror("Error creating destination file");
        close(src_fd);
        return -1;
    }

    char buffer[4096];
    ssize_t read_bytes, write_bytes;

    while ((read_bytes = read(src_fd, buffer, sizeof(buffer))) > 0) {
        write_bytes = write(dest_fd, buffer, read_bytes);
        if (write_bytes != read_bytes) {
            perror("Error writing to destination file");
            close(src_fd);
            close(dest_fd);
            return -1;
        }
    }

    close(src_fd);
    close(dest_fd);

    return 0;
}

static int xmp_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = creat(fpath, mode);
    if (fd == -1)
        return -errno;

    fi->fh = fd;

    log_operation("CREATE", path);

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int fd = open(fpath, fi->flags);
    if (fd == -1)
        return -errno;

    fi->fh = fd;

    log_operation("OPEN", path);
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = pwrite(fi->fh, buf, size, offset);
    if (res == -1)
        res = -errno;

    log_operation("WRITE", path);
    return res;
}

static int xmp_unlink(const char *path)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = unlink(fpath);
    if (res == -1)
        return -errno;

    log_operation("UNLINK", path);
    return 0;
}

static int xmp_utimens(const char *path, const struct timespec ts[2])
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = utimensat(0, fpath, ts, AT_SYMLINK_NOFOLLOW);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_truncate(const char *path, off_t size)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    int res = truncate(fpath, size);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_flush(const char *path, struct fuse_file_info *fi)
{
    (void)path;
    int res = close(fi->fh);
    if (res == -1)
        return -errno;

    return 0;
}

static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
	char full_path[4096];
        sprintf(full_path, "%s/%s", fpath, de->d_name);

        // Cek apakah ini file
        if (S_ISREG(st.st_mode)) {
            // Implementasi pemindahan file sesuai dengan folder 
            if (is_extension(de->d_name, "pdf") || is_extension(de->d_name, "docx")) {
                xmp_mkdir("/documents", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/documents/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "jpg") || is_extension(de->d_name, "png") || is_extension(de->d_name, "ico")) {
                xmp_mkdir("/images", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/images/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "js") || is_extension(de->d_name, "html") || is_extension(de->d_name, "json")) {
                xmp_mkdir("/website", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/website/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "c") || is_extension(de->d_name, "sh")) {
                xmp_mkdir("/sisop", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/sisop/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "txt")) {
                xmp_mkdir("/text", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/text/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            } else if (is_extension(de->d_name, "ipynb") || is_extension(de->d_name, "csv")) {
                xmp_mkdir("/aI", 0755);
                char dest_path[4096];
                sprintf(dest_path, "%s/aI/%s", dirpath, de->d_name);
                if (move_file(full_path, dest_path) == -1) {
                    perror("Error moving file");
                    closedir(dp);
                    return -1;
                }
            }
        }
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if(strcmp(path,"/") == 0)
    {
        path=dirpath;

        sprintf(fpath,"%s",path);
    }
    else sprintf(fpath, "%s%s",dirpath,path);

    int res = 0;
    int fd = 0 ;

    (void) fi;

    fd = open(fpath, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    log_operation("READ", path);
    return res;
}

static void xmp_destroy(void *userdata) {
    (void) userdata;  

    fclose(log_file);  // Menutup file log
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rmdir = xmp_rmdir,
    .create = xmp_create,
    .open = xmp_open,
    .write = xmp_write,
    .unlink = xmp_unlink,
    .utimens = xmp_utimens,
    .truncate = xmp_truncate,
    .flush = xmp_flush,
    .destroy = xmp_destroy, 
};

int  main(int  argc, char *argv[])
{
    umask(0);

    open_log_file();

    return fuse_main(argc, argv, &xmp_oper, NULL);
}

