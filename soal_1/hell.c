#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

void reverseFileContents(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    fseek(file, 0, SEEK_END);
    long fileSize = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *buffer = (char *)malloc(fileSize + 1);
    if (buffer == NULL) {
        perror("Memory allocation error");
        exit(EXIT_FAILURE);
    }

    fread(buffer, 1, fileSize, file);
    fclose(file);

    for (long i = 0, j = fileSize - 1; i < j; i++, j--) {
        char temp = buffer[i];
        buffer[i] = buffer[j];
        buffer[j] = temp;
    }

    file = fopen(filename, "w");
    if (file == NULL) {
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
    }

    fwrite(buffer, 1, fileSize, file);
    fclose(file);

    free(buffer);
}

void reverseFileNameInDirectory(const char *dirPath) {
    DIR *dir = opendir(dirPath);
    if (dir == NULL) {
        perror("Error opening directory");
        exit(EXIT_FAILURE);
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            char oldFilePath[256];
            snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", dirPath, entry->d_name);

            char reversedName[256];
            strncpy(reversedName, entry->d_name, sizeof(reversedName) - 1);
            reversedName[sizeof(reversedName) - 1] = '\0';

            size_t len = strnlen(reversedName, sizeof(reversedName));
            for (size_t i = 0, j = (len < 20) ? len - 1 : 19; i < j; i++, j--) {
                char temp = reversedName[i];
                reversedName[i] = reversedName[j];
                reversedName[j] = temp;
            }

            char newFilePath[256];
            snprintf(newFilePath, sizeof(newFilePath), "%s/%s", dirPath, reversedName);

            if (rename(oldFilePath, newFilePath) == -1) {
                perror("Error reversing file name");
                closedir(dir);
                exit(EXIT_FAILURE);
            }
        }
    }

    closedir(dir);
}

void performSisopOperation() {
    // Mengubah permission pada file "script.sh" di dalam direktori "sisop"
    char scriptPath[] = "data/sisop/script.sh";
    if (chmod(scriptPath, 0666) == -1) {
        perror("Error changing permission");
        exit(EXIT_FAILURE);
    }

    // Membalikkan isi file dengan prefix "test" di dalam direktori "sisop"
    char testFilenames[][50] = {"data/sisop/test-hai.txt", "data/sisop/test-script.txt"};
    int numTestFiles = sizeof(testFilenames) / sizeof(testFilenames[0]);

    for (int i = 0; i < numTestFiles; i++) {
        reverseFileContents(testFilenames[i]);
    }

    printf("Sisop operation executed!\n");
}

int main(int argc, char *argv[]) {
    if (argc != 2 && argc != 3) {
        fprintf(stderr, "Usage: %s <filename> [<destination>]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *originalFileName = argv[1];
    char *destination = (argc == 3) ? argv[2] : NULL;

    char galleryPath[] = "data/gallery";
    char revTestPath[] = "data/gallery/rev-test";
    char deleteFotoPath[] = "data/gallery/delete-foto";

    // Pengecekan apakah argumen terakhir adalah "sisop"
    if (argc == 2 && strcmp(originalFileName, "sisop") == 0) {
        // Mengeksekusi operasi pada direktori "sisop"
        performSisopOperation();
        return 0;
    }

    // Pengecekan apakah argumen terakhir adalah "rev-test" atau "delete-foto"
    if (destination != NULL) {
        if (strcmp(destination, "rev-test") == 0) {
            // Mengeksekusi pemindahan dan pembalikan nama file pada folder "rev-test"

            if (access(revTestPath, F_OK) == -1) {
                if (mkdir(revTestPath, 0777) == -1) {
                    perror("Error creating rev-test directory");
                    exit(EXIT_FAILURE);
                }
            }

            char oldFilePath[256];
            snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", galleryPath, originalFileName);

            char newFilePath[256];
            snprintf(newFilePath, sizeof(newFilePath), "%s/%s", revTestPath, originalFileName);

            if (rename(oldFilePath, newFilePath) == -1) {
                perror("Error moving file");
                exit(EXIT_FAILURE);
            }

            reverseFileNameInDirectory(revTestPath);
        } else if (strcmp(destination, "delete-foto") == 0) {
            // Mengeksekusi pemindahan dan penghapusan file pada folder "delete-foto"

            if (access(deleteFotoPath, F_OK) == -1) {
                if (mkdir(deleteFotoPath, 0777) == -1) {
                    perror("Error creating delete-foto directory");
                    exit(EXIT_FAILURE);
                }
            }

            char oldFilePath[256];
            snprintf(oldFilePath, sizeof(oldFilePath), "%s/%s", galleryPath, originalFileName);

            char newFilePath[256];
            snprintf(newFilePath, sizeof(newFilePath), "%s/%s", deleteFotoPath, originalFileName);

            if (rename(oldFilePath, newFilePath) == -1) {
                perror("Error moving file");
                exit(EXIT_FAILURE);
            }

            if (remove(newFilePath) == -1) {
                perror("Error deleting file");
                exit(EXIT_FAILURE);
            }
        } else {
            fprintf(stderr, "Invalid destination folder.\n");
            exit(EXIT_FAILURE);
        }
    } else {
        fprintf(stderr, "Invalid usage.\n");
        exit(EXIT_FAILURE);
    }

    return 0;
}
